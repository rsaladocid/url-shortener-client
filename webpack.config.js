const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlWebpackPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = (env, argv) => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: "[name]_[local]_[hash:base64]",
              sourceMap: true,
              minimize: true
            }
          }
        ]
      }
    ]
  },
  devServer: {
    port: 8081
  },
  externals: {
    'Config': JSON.stringify(argv.mode === 'production' ? {
      serverUrl: "https://ancient-waters-34174.herokuapp.com",
      login: {
        email: "guest@app.com",
        password: "guestappcom"
      }
    } : {
      serverUrl: "http://localhost:8080",login: {
        email: "guest@app.com",
        password: "guestappcom"
      }
    })
  },
  plugins: [htmlWebpackPlugin]
});