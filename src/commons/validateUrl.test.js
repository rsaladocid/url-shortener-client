import validateUrl from './validateUrl';

describe('validateUrl()', () => {
    it('complete url', () => {
        let isValid = validateUrl('http://www.google.es');
        expect(isValid).toEqual(true);
    });

    it('complete url with invalid protocol', () => {
        let isValid = validateUrl('test://www.google.es');
        expect(isValid).toEqual(false);
    });

    it('url without protocol', () => {
        let isValid = validateUrl('www.google.es');
        expect(isValid).toEqual(true);
    });

    it('url with uppercase and without protocol', () => {
        let isValid = validateUrl('WWW.GOOGLE.ES');
        expect(isValid).toEqual(true);
    });

    it('invalid url', () => {
        let isValid = validateUrl('http://.es');
        expect(isValid).toEqual(false);
    });

    it('null url', () => {
        let isValid = validateUrl(null);
        expect(isValid).toEqual(false);
    });

    it('empty url', () => {
        let isValid = validateUrl('');
        expect(isValid).toEqual(false);
    });
});