import React from "react";
import { Segment, Grid, Divider} from "semantic-ui-react";
import isValidUrl from "./commons/validateUrl";
import Config from "Config";
import Header from "./components/Header/Header";
import Form from "./components/Form/Form";
import Message from "./components/Message/Message";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: undefined,
            urlToShorten: undefined,
            generating: false,
            invalidUrl: false,
            shortedUrl: undefined
        }
    }

    componentDidMount() {
        this.loginAsGuest();
    }

    loginAsGuest() {
        this._prepareAndSendLoginRequest(Config.login.email, Config.login.password);
    }

    _prepareAndSendLoginRequest(email, password) {
        const url = Config.serverUrl + '/login';

        const request = new XMLHttpRequest();
        request.open('POST', url, true);

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    const token = request.getResponseHeader('Authorization');
                    this.setState({ token });
                } else {
                    this.setState({ shortedUrl: null });
                }
            }
        };

        request.setRequestHeader('Content-Type', 'application/json');
        request.send(JSON.stringify({ email, password }));
    }

    handleFormChange(event) {
        this.setState({ urlToShorten: event.target.value });
    }

    handleFormSubmit() {
        this.setState({ invalidUrl: false });
        const urlToShorten = this.state.urlToShorten;

        if (isValidUrl(urlToShorten)) {
            this.shorten();
        } else {
            this.setState({ invalidUrl: true });
        }
    }

    shorten() {
        this._startGeneratingShortUrl();
        this._prepareAndSendShortenRequest();
    }

    _startGeneratingShortUrl() {
        this.setState({ generating: true, shortUrl: undefined });
    }

    _prepareAndSendShortenRequest() {
        const url = Config.serverUrl + '/urls';

        const request = new XMLHttpRequest();
        request.open("POST", url, true);
        
        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    this.setState({ shortedUrl: JSON.parse(request.responseText) });
                } else {
                    this.setState({ shortedUrl: null });
                }
        
                this._stopGeneratingShortUrl();
            }
        };

        request.setRequestHeader("Content-Type", "application/json");
        request.setRequestHeader("Authorization", this.state.token);
        const requestMessage = this._createShortenRequestMessage();
        request.send(JSON.stringify(requestMessage));
    }

    _stopGeneratingShortUrl() {
        this.setState({ generating: false });
    }

    _createShortenRequestMessage() {
        if (this._isProtocolDefined(this.state.urlToShorten)) {
            return {
                url: this.state.urlToShorten
            }
        } else {
            return {
                url: "http://" + this.state.urlToShorten
            }
        }
    }

    _isProtocolDefined(url) {
        return url.startsWith("http://") || url.startsWith("https://");
    }

    render() {
        return <Grid centered columns={1}>
            <Grid.Column largeScreen={8} widescreen={16}>
                <Header color="blue" title="URL Shortener" />
                <Segment>
                    <Form invalid={this.state.invalidUrl} onSubmit={() => this.handleFormSubmit()} onChange={(event) => this.handleFormChange(event)} />
                    <Message shortedUrl={this.state.shortedUrl} generating={this.state.generating} />
                </Segment>
            </Grid.Column>
        </Grid>;
    }
}

export default App;