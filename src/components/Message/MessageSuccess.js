import React from "react";
import { Message, Icon, Divider, Grid, Transition } from "semantic-ui-react";
import ActionBar from "../ActionBar/ActionBar";
import InfoBar from "../InfoBar/InfoBar";

export default ({shortUrl, longUrl, textToCopy, urlToOpen, numberOfClicks, createdDate}) =>
    <Message icon positive>
        <Transition animation='tada' transitionOnMount={true}>
            <Icon name='linkify' />
        </Transition>
        <Message.Content>
            <Message.Header>{shortUrl}</Message.Header>
            {longUrl}
            <Divider hidden />
            <ActionBar textToCopy={textToCopy} urlToOpen={urlToOpen} />
            <Divider hidden />
            <InfoBar numberOfClicks={numberOfClicks} date={createdDate} />
        </Message.Content>
    </Message>;