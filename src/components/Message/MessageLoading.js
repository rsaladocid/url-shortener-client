import React from "react";
import { Message, Icon } from "semantic-ui-react";

export default ({title, message}) =>
    <Message icon>
      <Icon name='circle notched' loading />
      <Message.Content>
        <Message.Header>{title}</Message.Header>
        {message}
      </Message.Content>
    </Message>;