import React from "react";
import { Message, Icon, Transition } from "semantic-ui-react";

export default ({title, message}) =>
    <Message icon negative>
        <Transition animation='tada' transitionOnMount={true}>
            <Icon name='bolt' />
        </Transition>
        <Message.Content>
            <Message.Header>{title}</Message.Header>
            {message}
        </Message.Content>
    </Message>;