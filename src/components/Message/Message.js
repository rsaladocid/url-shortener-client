import React from "react";
import MessageLoading from "./MessageLoading";
import MessageSuccess from "./MessageSuccess";
import MessageError from "./MessageError";

export default ({shortedUrl, generating}) => {
  return <div>
    {generating &&
    <MessageLoading
      title="Just one second"
      message="We are generating a short link for you." />
    }
    {!generating && shortedUrl === null &&
    <MessageError
      title="Oops!"
      message="Error connecting to server. Please try again." />
    }
    {!generating && shortedUrl &&
    <MessageSuccess
      shortUrl={shortedUrl.shortUrl}
      longUrl={shortedUrl.longUrl}
      textToCopy={shortedUrl.shortUrl}
      urlToOpen={shortedUrl.shortUrl}
      numberOfClicks={shortedUrl.numberOfClicks}
      createdDate={shortedUrl.createdDate} />
    }
  </div>
}