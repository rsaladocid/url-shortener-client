import React from "react";
import { Icon, Header } from "semantic-ui-react";

export default ({title, color}) => 
    <Header as='h2' icon textAlign='center'>
        <Icon name='linkify' circular inverted color={color} />
        <Header.Content>{title}</Header.Content>
    </Header>;