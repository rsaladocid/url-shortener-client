import React from "react";
import { Form, Label, Icon, Button, Grid } from "semantic-ui-react";

export default ({invalid, onSubmit, onChange}) =>
    <Form onSubmit={onSubmit}>
        <Form.Group widths='equal'>
            <Form.Field>
                <Form.Input fluid placeholder="Put a link to shorten it" onChange={onChange} />
                {invalid &&
                <Label basic color='red' pointing>
                    Please enter a valid URL
                </Label>
                }
            </Form.Field>
        </Form.Group>
        <Grid centered>
            <Grid.Row>
                <Form.Group widths='equal'>
                    <Form.Button animated='vertical' primary>
                        <Button.Content visible>Shorten</Button.Content>
                        <Button.Content hidden>
                            <Icon name='linkify' />
                        </Button.Content>
                    </Form.Button>
                </Form.Group>
            </Grid.Row>
        </Grid>
    </Form>;