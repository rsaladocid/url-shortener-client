import React from "react";
import { Label, Icon } from "semantic-ui-react";

export default ({numberOfClicks}) =>
    <Label size='tiny'>
      <Icon name='chart bar outline' /> {numberOfClicks ? numberOfClicks : 0}
    </Label>;
