import React from "react";
import Clicks from "./InfoClicks";
import Date from "./InfoDate";
import { Label, Grid } from "semantic-ui-react";

export default ({numberOfClicks, date}) =>
    <Grid>
        <Grid.Column textAlign='right'>
            <Label.Group size='tiny'>
                <Clicks numberOfClicks={numberOfClicks} />
                <Date date={date} />
            </Label.Group>
        </Grid.Column>
    </Grid>;