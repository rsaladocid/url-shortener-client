import React from "react";
import { Label, Icon } from "semantic-ui-react";

export default ({date}) =>
    <Label size='tiny'>
        <Icon name='calendar outline' /> {date ? date.split("T")[0] : ""}
    </Label>;
