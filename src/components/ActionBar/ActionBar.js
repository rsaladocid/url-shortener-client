import React from "react";
import Copy from "./CopyAction";
import Open from "./OpenAction";

export default ({textToCopy, urlToOpen}) =>
    <div>
        <Copy text={textToCopy} />
        <Open url={urlToOpen} />
    </div>;
