import React from "react";
import { Button } from "semantic-ui-react";
import copy from 'copy-to-clipboard';

class CopyAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: props.text,
            alert: false
        }
    }

    handleCopy() {
        copy(this.state.text);
        this._alert();
    }

    _alert() {
        this.setState({alert: true});
        setTimeout(() => this.setState({alert: false}), 3000);
    }

    render() {
        if (this.state.alert === true) {
            return <Button inverted size='mini' color='green' content='COPIED!' />;
        } else {
            return <Button basic size='mini' color='grey' content='COPY' onClick={() => this.handleCopy()} />;
        }
    }
}

export default CopyAction;
