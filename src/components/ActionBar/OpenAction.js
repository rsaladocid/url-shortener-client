import React from "react";
import { Button } from "semantic-ui-react";

class OpenAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: props.url
        }
    }

    handleOpen() {
        window.open(this.state.url);
    }

    render() {
        return <Button basic size='mini' color='grey' content='OPEN' onClick={() => this.handleOpen()} />;
    }
}

export default OpenAction;